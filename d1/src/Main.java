import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");
        //Operators in Java

        //Arithmetic: +, -, *, /, %
        //Comparison: >, <, >=, <=, ==(equal), !=(not equal)
        //Logical: &&, ||, !(not)
        //Assignment and Reassignment: =

        //Conditional Control Structure in Java
        //Statements allows us to manipulate the flow of the code depending on the evaluation of the condition

        /*
        Syntax:
            if(condition){
            }
       */

        int num1 = 15;

        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }

        //else statement will allow us to run a task or code if the IF condition receives a falsy value

        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        //Mini Activity
        //Create/Instatiate a new scanner obj and name it as numberScanner.
        //Ask the user for an integer and store the input in a variable.
        //Add an if-else statement:
            //IF the number is even, show a message:
                //"num is even"
            //ELSE show the ff meesage:
                //"num is odd"

        Scanner numberScanner = new Scanner(System.in);
//        System.out.println("Enter a Number");
//        double intInput = numberScanner.nextInt();
//
//        if(intInput % 2 == 0){
//            System.out.println("number is even");
//        } else {
//            System.out.println("number is odd");
//        }
/*
    Switch Cases
        Switch Statements are control flow structures that allow one code block to be run out of many other code blocks.
        We compare the given value against each cases. And if a case matches, we will run that code block.
        This is mostly used if the user input is predictable.

        Default case is added to run a task/code in the event that there is no match
*/
//        System.out.println("Enter a number from 1-4 to see one of the four directions");
//        int directionValue = numberScanner.nextInt();
//
//        switch(directionValue){
//
//            case 1:
//                System.out.println("SM North Edsa");
//                break;
//            case 2:
//                System.out.println("SM SouthMall");
//                break;
//            case 3:
//                System.out.println("SM City Taytay");
//                break;
//            case 4:
//                System.out.println("SM Manila");
//                break;
//            default:
//                System.out.println("Out of Range");
//        }

        //Arrays
        //Arrays are objects that can contain data. Howver, in Java, arrays have a fixed/limited number of values with the same data type.
        //Unlike in JS, the length of Java arrays are established when the array is created.

        /* Syntax:
            dataType[] arrName = new dataType[numberOfElements];
            or
            dataType[] arrName = {elementA,elementB...};
        */

        String[] newArr = new String[3];
        newArr[0] = "Clark";
        newArr[1] = "Bruce";
        newArr[2] = "Diana";
        //newArr[3] = "Barry";
        //newArr[2] = 25;

        //This will show the memory address of our array or the location of our array within the memory:
        System.out.println(newArr);

        //To display the actual values of the array, we have to first convert it to a string using the Arrays class from Java
        //toString() is used to show values of the array as a string in the terminal
        System.out.println(Arrays.toString(newArr));

        //Array Methods
        //toString() - retrieves the actual value of the array as a string

        //Sort Method
        Arrays.sort(newArr);
        System.out.println("Results of the Arrays.sort()");
        System.out.println(Arrays.toString(newArr));

        Integer[] intArr = new Integer[3];
        intArr[0] = 54;
        intArr[1] = 12;
        intArr[2] = 67;
        System.out.println("Initial Order of the intArray:");
        System.out.println(Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Order of items in intArr after sort():");
        System.out.println(Arrays.toString(intArr));

        //binarySearch() - allows us to pass an argument/item to search within our array. binarySearch() will then return index number of the found element.
        //You can use a scanner to get input for your search terms

        String searchTerm = "Bruce";
        int result = Arrays.binarySearch(newArr,searchTerm);
        System.out.println("The index of " + searchTerm + " is " + result);

        //Array with Initialized Value:
        String[] arrSample = {"Tony", "Thor", "Steve"};
        System.out.println(Arrays.toString(arrSample));
        //arrSample[3] = "Peter";

/*      Array List
        Array Lists are resizable collections/arrays that functions similarly to how arrays work in JS.
        Using the new keyword in creating an ArrayList does not require datatype of the array list to be defined to avoid repetition
*/
        //Syntax:
        //ArrayList<dataType> arrName = new ArrayList<>();
        ArrayList<String> students = new ArrayList<>();

        //ArrayList Methods
        //arrayListName.add(<itemToAdd>) - adds elements in our array list:
        students.add("Paul");
        students.add("John");
        System.out.println(students);

        //arrayListName.get(index) - retrieve items from the array list using its index
        System.out.println(students.get(1));

        //arrayListName.set(index,value) - update an item by its index
        students.set(0,"George");
        System.out.println(students);

        //arrayListName.remove(index) - remove an item from the array list by its index
        students.remove(1);
        System.out.println(students);

        students.add("Ringo");

        //arrayList.clear() - removes all items from the array list
        students.clear();
        System.out.println(students);

        //arrayListName.size() - gets the length of our arrayList
        System.out.println(students.size());

        students.add("James");
        students.add("Wade");
        students.add("Bosh");

        System.out.println(students.size());

        //Array List with Initialized Values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates","Elon Musk","Jeff Bezos"));
        System.out.println(employees);

        //HashMaps
        //Most objects in Java are defined and are instantations of Classes that contains a proper set of properties and methods. There might be use cases whete it is not appropriate or you may simply want to store a collection of data in key-value pairs.
        //In Java, "keys" are also referred as "fields"
        //HashMaps offers flexibility when storing a collection of data.
        /* Syntax
            HashMap<fieldDataType,valueDataType> varName = new HashMap<>();
        */

        HashMap<String,String> userRoles = new HashMap<>();

        //Add new field and values in the hashmap
        //hashMapName.put(<field>,<value>);
        userRoles.put("Anna","Admin");
        userRoles.put("Alice","User");
        System.out.println(userRoles);
        userRoles.put("Alice","Teacher");
        System.out.println(userRoles);
        userRoles.put("Dennis","User");
        System.out.println(userRoles);

        //retrieves values by fields
        //hashMapName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("alice"));
        System.out.println(userRoles.get("Ellen"));

        //removes an element/field-value
        //hashMapName.remove("field");
        userRoles.remove("Dennis");
        System.out.println(userRoles);

        //retrieve hashMap keys
        //hashMapName.keySet();
        System.out.println(userRoles.keySet());
    }
}