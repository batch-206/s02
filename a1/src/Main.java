import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] fruits = new String[5];
        fruits[0] = "Apple";
        fruits[1] = "Avocado";
        fruits[2] = "Banana";
        fruits[3] = "Kiwi";
        fruits[4] = "Orange";

//Array
        System.out.println("Fruits in Stock: " + Arrays.toString(fruits));

        Scanner fruitScanner = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String inputFruit = fruitScanner.nextLine();

        int result = Arrays.binarySearch(fruits,inputFruit);
        System.out.println("The index of " + inputFruit + " is: " + result);

//ArrayList
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Irene");
        friends.add("Seulgi");
        friends.add("Taeyeon");
        friends.add("IU");

        System.out.println("My friends are: " + friends);

//HashMap
        HashMap<String,Integer> inventory = new HashMap<>();

        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);

    }
}